<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Navigator</title>
    <link rel="stylesheet" href="./css/main.css">
    <link rel="shortcut icon" href="./images/favicon.png" type="image/png">
    <style>
        .mapouter{
            position:relative;
            text-align:right;
            width: 100%;
            height: 428px;
        }
        .gmap_canvas {
            overflow:hidden;
            background:none!important;
            width: 100%;
            height: 428px;
        }
        .gmap_canvas iframe {
            width: 100%;
            height: 100%;
        }
        .mapouter:after {
            content: '';
            position: absolute;
            width: 15px;
            height: 77px;
            left: 100%;
            top: 15%;
            background-color: #5d39e3;
        }
    </style>
</head>
<body>
    <canvas style="position: absolute; top: 0; left: 0; transform: translate(0px, 0px); z-index: 10000; pointer-events: none"></canvas>
    <canvas style="position: absolute; top: 0; left: 0; transform: translate(0px, 0px); z-index: 10000; pointer-events: none"></canvas>
    <div class="home" id="home">
        <div class="container">
            <div class="row py-3 header">
                <div class="col-lg-2 col-6">
                    <a href="#">
                        <img src="./images/SecuritySystem.png" alt="Navigator-SG" class="logo">
                    </a>
                </div>
                <div class="offset-lg-1"></div>
                <div class="col-lg-4 col-6">
                    <nav class="main-nav">
                        <ul>
                            <li>
                                <a href="#home" class="active">Главная</a>
                            </li>
                            <li>
                                <a href="#about">О компании</a>
                            </li>
                            <li>
                                <a href="#services">Услуги</a>
                            </li>
                            <li>
                                <a href="#reviews">Отзывы</a>
                            </li>
                            <li>
                                <a href="#contacts">Контакты</a>
                            </li>
                        </ul>
                    </nav>
                    <a href="#" class="toggle hc-offcanvas-trigger">
                        <span></span>
                    </a>
                </div>
                <div class="offset-lg-1"></div>
                <div class="col-lg-2 col-6">
                    <button type="button" class="feedback-btn">Обратная связь</button>
                </div>
                <div class="col-lg-2 col-6">
                    <div class="form__bc"></div>
                    <form action="" class="search-form">
                        <input type="submit" class="search-btn" value="">
                        <input type="search" class="search-field" name="search" placeholder="Поиск" autocomplete="off">
                    </form>
                </div>
            </div>
            <div class="air-top"></div>
            <div class="row">
                <div class="offset-lg-1"></div>
                <div class="col-lg-8 col-12" data-aos="fade-up">
                    <div class="title-group">
                        <h1 class="title">Охранное агенство Navigator-SG</h1>
                        <p class="description">
                            Мы предлагаем клиентам только надежные и проверенные решения для организации охраны объектов. Специалисты компании оказывают помощь в подборе наиболее подходящего варианта для конкретного случая. 
                        </p>
                    </div>
                </div>
                <div class="offset-lg-3"></div>
            </div>
        </div>
    </div>
    <div class="about" id="about">
        <div class="container">
            <div class="air-top"></div>
            <div class="row">
                <div class="offset-lg-1"></div>
                <div class="col-lg-5 col-12" data-aos="fade-right">
                    <h1 class="title">О компании</h1>
                    <h2 class="subtitle">Охранное агентство Navigator-SG предлагает широкий комплекс охранных систем и услуг в Алматы и по всей территории РК.</h2>
                    <p class="description">
                    Охрана квартир, домов, офисов и коттеджей по самым приемлемым ценам.
Navigator-SG – одна из крупнейших компаний–поставщиков услуг в Алматы и всем регионе в области обеспечения охраны различных объектов. Мы продаем, монтируем, подключаем и обслуживаем охранные системы любых типов, как для бытовых, так и для промышленных объектов.
                    </p>
                    <p class="description">
                    Где-то лучше подойдёт пультовая охрана, где-то оптимальным вариантом будет организация видеонаблюдения или же использование активных охранных систем.
                    </p>
                </div>
                <div class="col-lg-5 col-12" style="z-index: 100000" data-aos="fade-left">
                    <div class="video__wrapper">
                        <video width="455" height="379" loop playsinline poster="./images/poster.png">
                            <source src="./videos/mov_bbb.mp4" type="video/mp4" codecs="avc1.42E01E, mp4a.40.2">
                        </video>
                        <button type="button" class="show__video">Посмотреть видео</button>
                    </div>
                </div>
                <div class="offset-lg-1"></div>
            </div>
        </div>
    </div>
    <div class="advantages" id="advantages">
        <div class="container">
            <div class="air-top"></div>
            <div class="row">
                <div class="offset-lg-4"></div>
                <div class="col-lg-4 d-flex flex-nowrap justify-content-center">
                    <h1 class="title">Преимущества</h1>
                </div>
                <div class="offset-lg-4"></div>
            </div>
            <div class="row my-lg-5 my-0">
                <div class="offset-lg-1"></div>
                <div class="col-lg-2 col-12 py-lg-0 py-4 d-flex flex-nowrap align-items-end justify-content-between" data-aos="fade-up" data-aos-duration="500">
                        <span class="count">01</span>
                        <p class="description">Широкий спектр услуг</p>
                </div>
                <div class="offset-lg-1"></div>
                <div class="col-lg-3 col-12 py-lg-0 py-4 d-flex flex-nowrap align-items-end justify-content-between" data-aos="fade-up" data-aos-duration="1000">
                        <span class="count">02</span>
                        <p class="description">Высококвалифицированные специалисты</p>
                </div>
                <div class="offset-lg-1"></div>
                <div class="col-lg-3 col-12 py-lg-0 py-4 d-flex flex-nowrap align-items-end justify-content-between" data-aos="fade-up" data-aos-duration="1500">
                        <span class="count">03</span>
                        <p class="description">Служба технической поддержки</p>
                </div>
            </div>
            <div class="row my-lg-5 my-0">
                <div class="offset-lg-3"></div>
                <div class="col-lg-3 col-12 py-lg-0 py-4 d-flex flex-nowrap align-items-end justify-content-between" data-aos="fade-up" data-aos-duration="2000">
                        <span class="count">04</span>
                        <p class="description">Индивидуальный подход </p>
                </div>
                <div class="offset-lg-1"></div>
                <div class="col-lg-3 col-12 py-lg-0 py-4 d-flex flex-nowrap align-items-end justify-content-between" data-aos="fade-up" data-aos-duration="2500">
                        <span class="count">05</span>
                        <p class="description">Гибкая система оплаты </p>
                </div>
                <div class="offset-lg-2"></div>
            </div>
            <div class="row">
                <div class="offset-lg-5"></div>
                <div class="col-lg-2 col-12 p-lg-0">
                    <a href="#" class="show__services">Посмотреть услуги</a>
                </div>
                <div class="offset-lg-5"></div>
            </div>
        </div>
    </div>
    <div class="services" id="services">
        <div class="container">
            <div class="air-top"></div>
            <div class="row">
                <div class="offset-lg-1"></div>
                <div class="col-lg-11 col-12">
                    <h1 class="title">Наши услуги</h1>
                </div>
            </div>
            <div class="row">
                <div class="offset-lg-1"></div>
                <div class="col-lg-10 col-12" style="z-index: 100000">
                    <div class="description-row">
                        <div class="description-col" data-aos="fade-down" data-aos-duration="1000">
                            <div class="d-flex flex-lg-nowrap flex-wrap align-items-center h-100 mx-3">
                                <i class="fal fa-signal-stream"></i>
                                <p class="description"><span>Охранно-тревожная</span> сигнализация (отс)</p>
                            </div>
                        </div>
                        <div class="description-col active-col" data-aos="fade-down" data-aos-duration="1000">
                            <div class="d-flex flex-wrap align-items-center h-100 mx-3">
                                <i class="fal fa-layer-group"></i>
                                <p class="description"><span>Бронирование</span> cтекол</p>
                            </div>
                        </div>
                        <div class="description-col" data-aos="fade-down" data-aos-duration="1000">
                            <div class="d-flex flex-lg-nowrap flex-wrap align-items-center h-100 mx-3">
                                <i class="fal fa-cog"></i>
                                <p class="description"><span>Техническое</span> обслуживание</p>
                            </div>
                        </div>
                        <div class="description-col" data-aos="fade-down" data-aos-duration="1000">
                            <div class="d-flex flex-lg-nowrap flex-wrap align-items-center h-100 mx-3">
                                <i class="fad fa-sliders-v"></i>
                                <p class="description"><span>Установка</span> шлагбаумов</p>
                            </div>
                        </div>
                        <div class="description-col" data-aos="fade-down" data-aos-duration="1000">
                            <div class="d-flex flex-lg-nowrap flex-wrap align-items-center h-100 mx-3">
                                <i class="fal fa-unlock"></i>
                                <p class="description"><span>Системы</span> контроля доступа</p>
                            </div>
                        </div>
                        <div class="description-col" data-aos="fade-down" data-aos-duration="1000">
                            <div class="d-flex flex-lg-nowrap flex-wrap align-items-center h-100 mx-3">
                                <i class="fal fa-shield"></i>
                                <p class="description"><span>Пожарная</span> сигнализация</p>
                            </div>
                        </div>
                        <div class="description-col" data-aos="fade-down" data-aos-duration="1000">
                            <div class="d-flex flex-lg-nowrap flex-wrap align-items-center h-100 mx-3">
                                <i class="fal fa-video"></i>
                                <p class="description"><span>Система</span> видеонаблюдения</p>
                            </div>
                        </div>
                        <div class="description-col" data-aos="fade-down" data-aos-duration="1000">
                            <div class="d-flex flex-lg-nowrap flex-wrap align-items-center h-100 mx-3">
                                <i class="fal fa-scrubber"></i>
                                <p class="description"><span>СКС и ВОЛС</span></p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="offset-lg-1"></div>
            </div>
        </div>
    </div>
    <div class="reviews" id="reviews">
        <div class="container">
            <div class="air-top"></div>
            <div class="row">
                <div class="offset-lg-1"></div>
                <div class="col-lg-11 col-12 d-flex justify-content-center">
                    <h1 class="title">Отзывы клиентов</h1>
                </div>
            </div>
            <div class="row">
                <div class="offset-lg-1"></div>
                <div class="col-lg-10 col-12 reviews__carousel owl-carousel">
                    <div class="item review__item">
                        <div class="container">
                            <div class="row">
                                <div class="col-lg-7 d-flex justify-content-between align-items-center">
                                    <div class="avatar"></div>
                                    <p class="title"><span>Александр Ткачук</span>21 января 2019 года</p>
                                </div>
                            </div>
                            <div class="row mt-3">
                                <div class="col-lg-12">
                                    <p class="description">Охрана квартир, домов, офисов и коттеджей по самым приемлемым ценам.
                                    х компаний–поставщиков услуг в Алматы и всем регионе в области обеспечения охраны различных объектов. Мы продаем, монтируем, подключаем и обслуживаем охранные системы любых типов, как для бытовых, так и для промышленных объектов.</p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="item review__item">
                        <div class="container">
                            <div class="row">
                                <div class="col-lg-7 d-flex justify-content-between align-items-center">
                                    <div class="avatar"></div>
                                    <p class="title"><span>Александр Ткачук</span>21 января 2019 года</p>
                                </div>
                            </div>
                            <div class="row mt-3">
                                <div class="col-lg-12">
                                    <p class="description">Охрана квартир, домов, офисов и коттеджей по самым приемлемым ценам.
                                    х компаний–поставщиков услуг в Алматы и всем регионе в области обеспечения охраны различных объектов. Мы продаем, монтируем, подключаем и обслуживаем охранные системы любых типов, как для бытовых, так и для промышленных объектов.</p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="item review__item">
                        <div class="container">
                            <div class="row">
                                <div class="col-lg-7 d-flex justify-content-between align-items-center">
                                    <div class="avatar"></div>
                                    <p class="title"><span>Александр Ткачук</span>21 января 2019 года</p>
                                </div>
                            </div>
                            <div class="row mt-3">
                                <div class="col-lg-12">
                                    <p class="description">Охрана квартир, домов, офисов и коттеджей по самым приемлемым ценам.
                                    х компаний–поставщиков услуг в Алматы и всем регионе в области обеспечения охраны различных объектов. Мы продаем, монтируем, подключаем и обслуживаем охранные системы любых типов, как для бытовых, так и для промышленных объектов.</p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="item review__item">
                        <div class="container">
                            <div class="row">
                                <div class="col-lg-7 d-flex justify-content-between align-items-center">
                                    <div class="avatar" src="./images/review__image.png"></div>
                                    <p class="title"><span>Александр Ткачук</span>21 января 2019 года</p>
                                </div>
                            </div>
                            <div class="row mt-3">
                                <div class="col-lg-12">
                                    <p class="description">Охрана квартир, домов, офисов и коттеджей по самым приемлемым ценам.
                                    х компаний–поставщиков услуг в Алматы и всем регионе в области обеспечения охраны различных объектов. Мы продаем, монтируем, подключаем и обслуживаем охранные системы любых типов, как для бытовых, так и для промышленных объектов.</p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="item review__item">
                        <div class="container">
                            <div class="row">
                                <div class="col-lg-7 d-flex justify-content-between align-items-center">
                                    <div class="avatar" src="./images/review__image.png"></div>
                                    <p class="title"><span>Александр Ткачук</span>21 января 2019 года</p>
                                </div>
                            </div>
                            <div class="row mt-3">
                                <div class="col-lg-12">
                                    <p class="description">Охрана квартир, домов, офисов и коттеджей по самым приемлемым ценам.
                                    х компаний–поставщиков услуг в Алматы и всем регионе в области обеспечения охраны различных объектов. Мы продаем, монтируем, подключаем и обслуживаем охранные системы любых типов, как для бытовых, так и для промышленных объектов.</p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="item review__item">
                        <div class="container">
                            <div class="row">
                                <div class="col-lg-7 d-flex justify-content-between align-items-center">
                                    <div class="avatar" src="./images/review__image.png"></div>
                                    <p class="title"><span>Александр Ткачук</span>21 января 2019 года</p>
                                </div>
                            </div>
                            <div class="row mt-3">
                                <div class="col-lg-12">
                                    <p class="description">Охрана квартир, домов, офисов и коттеджей по самым приемлемым ценам.
                                    х компаний–поставщиков услуг в Алматы и всем регионе в области обеспечения охраны различных объектов. Мы продаем, монтируем, подключаем и обслуживаем охранные системы любых типов, как для бытовых, так и для промышленных объектов.</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="offset-lg-1"></div>
            </div>
        </div>
    </div>
    <div class="contacts">
        <div class="container">
            <div class="air-top"></div>
            <div class="row">
                <div class="offset-lg-1"></div>
                <div class="col-lg-5 col-12 py-5 py-lg-0">
                    <h1 class="title">Контакты</h1>
                    <ul>
                        <li>
                            <div class="i__wrapper">
                                <i class="fal fa-phone-alt"></i>
                            </div>
                            <p class="description">
                                Телефон:
                                <a href="tel:+38(063) 877-02-68">+38(063) 877-02-68</a>
                            </p>
                        </li>
                        <li>
                            <div class="i__wrapper">
                                <i class="fal fa-map-marker-alt"></i>
                            </div>
                            <p class="description">
                                Адрес:
                                <span>п. Дубовое, ул. Уличная, д.1</span>
                            </p>
                        </li>
                        <li>
                            <div class="i__wrapper">
                                <i class="fal fa-envelope"></i>
                            </div>
                            <p class="description">
                                Email:
                                <a href="mailto:navigator_sg@gmail.com">navigator_sg@gmail.com</a>
                            </p>
                        </li>
                    </ul>
                </div>
                <div class="col-lg-5 col-12" style="z-index: 100000">
                    <div class="mapouter">
                        <div class="gmap_canvas">
                            <iframe id="gmap_canvas" src="https://maps.google.com/maps?q=%D0%94%D0%BE%D1%81%D1%82%D1%8B%D0%BA&t=&z=13&ie=UTF8&iwloc=&output=embed" frameborder="0" scrolling="no" marginheight="0" marginwidth="0"></iframe>
                        </div>
                    </div>
                </div>
                <div class="offset-lg-1"></div>
            </div>
        </div>
    </div>
    <div class="footer">
        <div class="container my-5">
            <div class="row">
                <div class="offset-lg-1"></div>
                <div class="col-lg-3 col-6">
                    <p class="copyright">NavigatorSG©2020 Все права защищены</p>
                    <ul class="socials">
                        <li>
                            <a href="#">
                                <i class="fab fa-youtube"></i>
                            </a>
                        </li>
                        <li>
                            <a href="#">
                                <i class="fab fa-instagram"></i>
                            </a>
                        </li>
                        <li>
                            <a href="#">
                                <i class="fab fa-facebook-f"></i>
                            </a>
                        </li>
                    </ul>
                </div>
                <div class="offset-lg-1"></div>
                <div class="col-lg-2 col-6">
                    <a href="#" class="feedback-btn">Обратная связь</a>
                </div>
                <div class="offset-lg-2"></div>
                <div class="col-lg-2 col-12 py-3 py-lg-0 d-flex align-items-center">
                    <p class="developedBy">
                        Разработано в <a href="https://a-lux.kz"><img src="../images/a-lux.png" alt="A-lux"></a>
                    </p>
                </div>
                <div class="offset-lg-1"></div>
            </div>
        </div>
    </div>
    <script src="./js/main.js"></script>
</body>
</html>